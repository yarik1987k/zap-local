'use strict';

const puppeteer = require('puppeteer');
const { MongoClient } = require('mongodb');

(async () => {
    // MongoDB connection URI
    const uri = 'mongodb+srv://yarik1987k:NUxciXiVuOeTte4E@kirshner.gmhjo8g.mongodb.net/?retryWrites=true&w=majority&appName=kirshner'; 
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    try {
        await client.connect();
        const database = client.db('kirshner'); // Replace with your database name
        const collection = database.collection('kirshner'); // Replace with your collection name

        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
            executablePath: puppeteer.executablePath()
        });
        const page = await browser.newPage();

        // Function to extract data from a single product page
        const extractProductData = async (productUrl, productId) => {
            try {
                await page.goto(productUrl, { waitUntil: 'networkidle2' });

                return await page.evaluate((productId) => {
                    const slug = window.location.pathname.split('/').filter(Boolean).pop();
                    const title = document.querySelector('h1.product_title')?.innerText || 'No Title';
                    const featuredImage = document.querySelector('.woocommerce-product-gallery__image img')?.src || 'No Image';
                    const priceText = document.querySelector('.price')?.innerText || 'No Price';
                    const price = parseFloat(priceText.replace(/[^\d.]/g, '')) || 0; // Remove symbols and convert to number
                    const categories = Array.from(document.querySelectorAll('.posted_in a')).map(category => category.innerText);

                    // Check for sale labels
                    let saleLabel = null;
                    if (categories.includes("מבצע מוצר אחד ב 5 שלוש ב 10")) {
                        saleLabel = "Sale: 1 for 5, 3 for 10";
                    } else if (categories.includes("מבצע 2 ב 15 !!!")) {
                        saleLabel = "Sale: 2 for 15";
                    }

                    return { id: productId, slug, title, image: featuredImage, price, categories, saleLabel };
                }, productId);
            } catch (error) {
                console.error(`Error extracting product data from ${productUrl}:`, error);
                return null;
            }
        };

        // Function to extract product URLs from the shop page
        const extractProductUrls = async () => {
            return await page.evaluate(() => {
                const productLinks = Array.from(document.querySelectorAll('.products .product a.woocommerce-LoopProduct-link'));
                return productLinks.map(link => link.href);
            });
        };

        // Navigate through all pages of products and extract data
        const allProductData = [];
        const maxPages = 12;
        const categoryMapping = {};
        let categoryIdCounter = 1;
        let productIdCounter = 1;

        for (let currentPage = 1; currentPage <= maxPages; currentPage++) {
            const shopPageUrl = currentPage === 1 
                ? 'https://meshek-kirshner.co.il/shop/' 
                : `https://meshek-kirshner.co.il/shop/page/${currentPage}/`;
            console.log('Navigating to:', shopPageUrl);
            await page.goto(shopPageUrl, { waitUntil: 'networkidle2' });

            const productUrls = await extractProductUrls();
            console.log(`Found ${productUrls.length} products on page ${currentPage}`);

            for (const productUrl of productUrls) {
                const productData = await extractProductData(productUrl, productIdCounter++);
                if (productData) {
                    // Assign unique IDs to categories
                    productData.categories = productData.categories.map(category => {
                        if (!categoryMapping[category]) {
                            categoryMapping[category] = categoryIdCounter++;
                        }
                        return { name: category, id: categoryMapping[category] };
                    });
                    allProductData.push(productData);
                }
            }
        }

        // Insert data into MongoDB
        if (allProductData.length > 0) {
            await collection.insertMany(allProductData);
            console.log('Data has been saved to MongoDB');
        } else {
            console.log('No data to save');
        }

        await browser.close();
    } catch (error) {
        console.error('Error:', error);
    } finally {
        await client.close();
    }
})();
