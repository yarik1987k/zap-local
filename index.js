const express = require('express');
const cors = require('cors');
const { MongoClient } = require('mongodb');
const { exec } = require('child_process'); // Add this line
const app = express();
const port = 3000;

app.use(express.json());
app.use(cors());

const uri = 'mongodb+srv://yarik1987k:NUxciXiVuOeTte4E@kirshner.gmhjo8g.mongodb.net/kirshner?retryWrites=true&w=majority';
const client = new MongoClient(uri, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true
});

let database, collection;

async function connectToDatabase() {
    try {
        await client.connect();
        database = client.db('kirshner'); // Replace with your database name
        collection = database.collection('kirshner'); // Replace with your collection name
        console.log('Connected to MongoDB');
    } catch (err) {
        console.error('Error connecting to MongoDB:', err);
    }
}

connectToDatabase();

let dataStore = [];

// Routes
app.get('/items', (req, res) => {
    res.json(dataStore);
});

app.post('/items', (req, res) => {
    const item = req.body;
    dataStore.push(item);
    res.status(201).json(item);
});

app.delete('/items/:id', (req, res) => {
    const id = req.params.id;
    dataStore = dataStore.filter(item => item.id !== id);
    res.status(204).send();
});

// New route to serve products from MongoDB
app.get('/products', async (req, res) => {
    if (!collection) {
        console.error('No collection found.');
        return res.status(500).json({ error: 'No collection found. Ensure the database connection is established.' });
    }

    try {
        const products = await collection.find({}).toArray();
        res.json(products);
    } catch (err) {
        console.error('Error reading from MongoDB:', err);
        res.status(500).json({ error: 'Failed to read products from MongoDB', details: err.message });
    }
});

// New route to run the scrapper.js script
app.get('/run-scraper', (req, res) => {
    exec('npm run scrape', (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return res.status(500).json({ error: `Script execution failed: ${error.message}` });
        }
        console.log(`stdout: ${stdout}`);
        console.error(`stderr: ${stderr}`);
        res.json({ message: 'Scraper script executed successfully', output: stdout });
    });
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
